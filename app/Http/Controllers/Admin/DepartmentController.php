<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class DepartmentController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function departmentlist()
    {
        $data=DB::table('departments')->latest()->get();
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'department_name' => 'required|unique:departments|max:55',
        ]);

        $data=array();
        $data['department_name']=$request->department_name;
        DB::table('departments')->insert($data);
        return response()->json('success',200);

    }
    //delete department
    public function destroy($id)
    {
        DB::table('departments')->where('id',$id)->delete();
        return response()->json('Deleted Department',200);

    }

    //edit form method
    public function edit($id)
    {
        $data=DB::table('departments')->where('id',$id)->first();
        return response()->json($data);
    }

    //update method 
    public function update(Request $request)
    {
        $data=array();
        $data['department_name']=$request->department_name;
        DB::table('departments')->where('id',$request->id)->update($data);
        return response()->json('success',200);
    }




}
