const department = () => import('./components/department/list.vue')
const home = () => import('./components/home.vue')


export const routes = [
    { name: 'home',path: '/home',component: home },
    { name: 'departmentlist',path: '/department',component: department },
   
]