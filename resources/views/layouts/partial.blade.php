
    <div class="all__content">
        <div id="primary_nav" class="g_blue toggle-leftbar">
            <div class="first__left">
                <div class="main__nav">
                    <ul>
                        <li data-menu="dashboardmenu"><a href="#" class=""><span class="fas fa-desktop"></span></a></li>
                        <li data-menu="filemenu"><a href="#" class=""><span class="fas fa-file-archive"></span></a></li>
                        <li data-menu="calendermenu"><a href="#" class=""><span class="fas fa-calendar-alt"></span></a></li>
                        <li data-menu="addressmenu"><a href="#" class=""><span class="fas fa-address-book"></span></a></li>
                        <li data-menu="foldermenu"><a href="#" class=""><span class="fas fa-folder-open"></span></a></li>
                        <li data-menu="settingmenu"><a href="#" class=""><span class="fas fa-cog"></span></a></li>
                        <div class="buttom__icon__div">
                            <li data-menu="settingmenu" class="buttom__icon">
                                <a href="#" class=" buttom__link"><img src="{{ asset("asset/asset/img/folder.png") }}" alt=""></a>

                                <div class="dp__down">
                                    <ul class="dp__ul">

                                        <li class="dp__link">
                                            <a href=""><span class="fas fa-book"></span> 
                                                <p class="dp__link__text">
                                                    Address book are here
                                                    <span>test</span>
                                                </p>
                                            </a>
                                        </li>
                                        <li class="dp__link">
                                            <a href=""><span class="fas fa-book"></span> 
                                                <p class="dp__link__text">
                                                    Address book are here
                                                    <span>test</span>
                                                </p>
                                            </a>
                                        </li>
                                        <li class="dp__link">
                                            <a href=""><span class="fas fa-book"></span> 
                                                <p class="dp__link__text">
                                                    Address book are here
                                                    <span>test</span>
                                                </p>
                                            </a>
                                        </li>
                                        <li class="dp__link">
                                            <a href=""><span class="fas fa-book"></span> 
                                                <p class="dp__link__text">
                                                    Address book are here
                                                    <span>test</span>
                                                </p>
                                            </a>
                                        </li>
                                        <li class="dp__link">
                                            <a href=""><span class="fas fa-book"></span> 
                                                <p class="dp__link__text">
                                                    Address book are here
                                                    <span>test</span>
                                                </p>
                                            </a>
                                        </li>
                                        <li class="dp__link">
                                            <a href=""><span class="fas fa-book"></span> 
                                                <p class="dp__link__text">
                                                    Address book are here
                                                    <span>test</span>
                                                </p>
                                            </a>
                                        </li>
                                        <li class="dp__link">
                                            <a href=""><span class="fas fa-book"></span> 
                                                <p class="dp__link__text">
                                                    Address book are here
                                                    <span>test</span>
                                                </p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </li>
                        </div>
                    </ul>
                </div>
            </div>

            <div class="category-bar">
                <div id="sidebar">
                    <!-- ===========================================DASHBOARD SIDEBAR=================== -->
                    <div id="dashboardmenu" class="file__toggle active">
                        <ul id="sidenav" class="accordion_mnu collapsible ui-sortable">
                            <li>
                                <router-link to="/home"><span class="nav_icon fas fa-desktop"></span>Dashboard</router-link>
                            </li>
                          
                            <li class="dropend" data-submenu="department">
                                <a href="#" class="dropdown-toggle" data-bs-toggle="dropdown"><span class="nav_icon fas fa-desktop"></span>Department</a>
                                <div class="sub-menu dropdown-menu sub-dp" id="department">
                                    <div class="sub-menu-width">
                                        <div class="model__close">
                                            <button type="button" class="btn-close close-model"></button>
                                        </div>
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-lg-1 col-md-2 col-sm-2 col-4 p-1">
                                                    <div class="switch_bar">
                                                        <router-link to="/department" class="bar-link">
                                                            <span><img src="asset/asset/img/chart.png" alt=""></span>
                                                            <p>
                                                               Department
                                                            </p>
                                                        </router-link>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 col-md-2 col-sm-2 col-4 p-1">
                                                    <div class="switch_bar">
                                                        <a href="" class="bar-link">
                                                            <span><img src="asset/asset/img/note.png" alt=""></span>

                                                            <p>
                                                                Section
                                                            </p>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-1 col-md-2 col-sm-2 col-4 p-1">
                                                    <div class="switch_bar">
                                                        <span class="notify-grin">30</span>
                                                        <a href="" class="bar-link">
                                                            <span><img src="asset/asset/img/user.png" alt=""></span>
                                                            <p>
                                                                Subsection
                                                            </p>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <router-link to="#"><span class="nav_icon fas fa-desktop"></span>Example 1</router-link>
                            </li>
                            <li>
                                <router-link to="#"><span class="nav_icon fas fa-desktop"></span>Example 2</router-link>
                            </li>
                            <li>
                                <router-link to="#"><span class="nav_icon fas fa-desktop"></span>Example 3</router-link>
                            </li>
                            <li>
                                <router-link to="#"><span class="nav_icon fas fa-desktop"></span>Example 4</router-link>
                            </li> 
                            <li>
                                <router-link to="#"><span class="nav_icon fas fa-desktop"></span>Example 5</router-link>
                            </li>

                        </ul>
                    </div>


                    <!-- ===========================================DASHBOARD SIDEBAR=================== -->

                    <!-- ===========================================FILE SIDEBAR=================== -->
                    <div id="filemenu" class="file__toggle">
                        <ul id="sidenav" class="accordion_mnu collapsible ui-sortable">
                            <li data-submenu="check">
                                <a href="#"><span class="nav_icon fas fa-desktop"></span> Dashboard</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-calendar-alt"></span>Calendar</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-address-book"></span>Address Book</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-folder-open"></span>Folder</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-desktop"></span> Dashboard</a>
                            </li>

                        </ul>
                    </div>
                    <!-- ===========================================FILE SIDEBAR=================== -->

                    <!-- ===========================================CALENDER SIDEBAR=================== -->
                    <div id="calendermenu" class="file__toggle">
                        <ul id="sidenav" class="accordion_mnu collapsible ui-sortable">
                            <li>
                                <a href="#"><span class="nav_icon fas fa-desktop"></span> Dashboard</a>
                            </li>

                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>


                            </li>
                        </ul>
                    </div>
                    <!-- ===========================================CALENDER SIDEBAR=================== -->
                    <!-- ===========================================CONACT SIDEBAR=================== -->
                    <div id="addressmenu" class="file__toggle">
                        <ul id="sidenav" class="accordion_mnu collapsible ui-sortable">
                            <li>
                                <a href="#"><span class="nav_icon fas fa-desktop"></span> Dashboard</a>
                            </li>

                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>

                        </ul>
                    </div>
                    <!-- ===========================================CANTACT SIDEBAR=================== -->
                    <!-- ===========================================FOLDER SIDEBAR=================== -->
                    <div id="foldermenu" class="file__toggle">
                        <ul id="sidenav" class="accordion_mnu collapsible ui-sortable">
                            <li>
                                <a href="#"><span class="nav_icon fas fa-desktop"></span> Dashboard</a>
                            </li>

                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>

                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                        </ul>
                    </div>
                    <!-- ===========================================FOLDER SIDEBAR=================== -->
                    <!-- ===========================================SETTING SIDEBAR=================== -->
                    <div id="settingmenu" class="file__toggle">
                        <ul id="sidenav" class="accordion_mnu collapsible ui-sortable">
                            <li>
                                <a href="#"><span class="nav_icon fas fa-desktop"></span> Dashboard</a>
                            </li>

                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>

                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                            <li>
                                <a href="#"><span class="nav_icon fas fa-file-archive"></span> File</a>
                            </li>
                        </ul>
                    </div>
                    <!-- ===========================================SETTING SIDEBAR=================== -->
                </div>
            </div>
        </div>



        <div class="main-woaper">
            <header>
                <div class="navigation">
                    <div class="panel__nav">
                        <div class=" top-menu">
                            <div class="logo__sec">
                                <a href="" class="logo">
                                    <img src="{{ asset("asset/asset/img/logo.png") }}" alt="logo" class="logo__img">
                                </a>
                            </div>
                            <div id="left_bar_toggle"><span class="fas fa-bars"></span></div>
                        </div>
                        <div class="notify-menu">
                            <div class="head__content__sec">
                                <ul class="head__cn">
                                    <li class="dropdown dp__top top-icon">

                                        <span class="notify">30</span>
                                        <a href="" class="" id="dropdownMenuButton1" data-bs-toggle="dropdown">
                                            <span class="fas fa-exclamation-circle"></span>
                                        </a>

                                        <ul class="dropdown-menu dropdown__main__menu " aria-labelledby="dropdownMenuButton1">

                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <a href="" class="btn__sub">View All</a>
                                        </ul>
                                    </li>
                                    <li class="dropdown dp__top">
                                        <span class="notify-grin">30</span>
                                        <a href="" class="top-icon" id="dropdownMenuButton1" data-bs-toggle="dropdown">
                                            <span class="fas fa-envelope"></span>

                                        </a>

                                        <ul class="dropdown-menu dropdown__main__menu " aria-labelledby="dropdownMenuButton1">

                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <li>
                                                <span class="fas fa-user dropdown__icon"></span> <a class="dropdown-item" href="#"> Lorem Ipsum is simply dummy text</a>
                                            </li>
                                            <a href="" class="btn__sub">View All</a>
                                        </ul>
                                    </li>
                                    <li class="top-icon ms-3"><a href="#"><span class="fas fa-user"></span></a></li>
                                    <li class="user_info  me-5">
                                        <span class="user_name">Administrator</span>
                                        <span><a href="#">Profile</a> | <a href="#">Settings</a> | <a href="#">Help?</a></span>
                                    </li>
                                    <li class="top-icon"><a href="#"><span class="fas fa-power-off"></span></a></li>

                                </ul>

                            </div>

                        </div>
                    </div>
                </div>
            </header>
            <router-view></router-view>
        </div>
    </div>