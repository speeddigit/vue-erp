<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{("asset/asset/css/fontawesome/css/all.css")}}">
    <link rel="stylesheet" href="{{("asset/asset/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{("asset/asset/css/selectize.css")}}">
    <link rel="stylesheet" href="{{("asset/asset/css/dropzone.css")}}">

    <link href="{{("asset/css/reset.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/typography.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/body.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/shCore.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/jquery.jqplot.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/jquery-ui-1.8.18.custom.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/data-table.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/form.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/ui-elements.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/wizard.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/sprite.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("asset/css/gradient.css")}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{("asset/asset/css/comon.css")}}">
    <link rel="stylesheet" href="{{("asset/asset/css/layout.css")}}">
    <link rel="stylesheet" href="{{("asset/asset/css/style.css")}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
</head>
<body>
    <div id="app">
    @guest
    @else
       {{-- <li class="nav-item">
           <router-link to="/department">Department</router-link>
       </li>
       
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }}
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </li> --}}
        @include('layouts.partial')
        
    @endguest
    </div>


    <script src="{{ asset('js/app.js') }}" ></script>
   
    <script src="{{ asset("asset/js/custom-scripts.js") }}"></script>
    <script src="{{ asset("asset/asset/js/main.js") }}"></script>
    <script src="{{ asset("asset/js/jquery-1.7.1.min.js") }}"></script>
    
    <script src="{{ asset("asset/asset/js/bootstrap.bundle.min.js") }}"></script>
    <script src="{{ asset("asset/asset/js/canvas-main.js") }}"></script>
    <script src="{{ asset("asset/asset/js/jquery.fontstar.js") }}"></script>
    <script src="{{ asset("asset/asset/js/dropzone.js") }}"></script>
    <script src="{{ asset("asset/asset/js/jquery.repeater.js") }}"></script>
    <script src="{{ asset("asset/asset/js/apexcharts.js") }}"></script>
    <script src="{{ asset("asset/asset/js/canvas-main.js") }}"></script>
    

    <script src="{{ asset("asset/js/jquery-ui-1.8.18.custom.min.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.ui.touch-punch.js")}}"></script>
    <script src="{{ asset("asset/js/chosen.jquery.js")}}"></script>
    <script src="{{ asset("asset/js/uniform.jquery.js")}}"></script>
    <script src="{{ asset("asset/js/bootstrap-dropdown.js")}}"></script>
    <script src="{{ asset("asset/js/sticky.full.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.noty.js")}}"></script>
    <script src="{{ asset("asset/js/selectToUISlider.jQuery.js")}}"></script>
    <script src="{{ asset("asset/js/fg.menu.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.tagsinput.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.cleditor.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.tipsy.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.peity.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.simplemodal.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.jBreadCrumb.1.1.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.colorbox-min.js")}}"></script>
    <script src="{{ asset("asset/js/bootstrap-colorpicker.js")}}"></script>
    <script src="{{ asset("asset/js/jquery.idTabs.min.js") }}"></script>
    <script src="{{ asset("asset/js/jquery.multiFieldExtender.min.js") }}"></script>
    <script src="{{ asset("asset/js/jquery.confirm.js") }}"></script>
    <script src="{{ asset("asset/js/accordion.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/autogrow.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/check-all.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/data-table.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/ZeroClipboard.js") }}"></script>
    <script src="{{ asset("asset/js/TableTools.min.js") }}"></script>
    <script src="{{ asset("asset/js/jeditable.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/accordion.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/duallist.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/easing.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/full-calendar.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/input-limiter.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/inputmask.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/iphone-style-checkbox.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/meta-data.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/quicksand.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/raty.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/smart-wizard.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/stepy.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/treeview.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/ui-accordion.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/vaidation.jquery.js") }}"></script>
    <script src="{{ asset("asset/js/mosaic.1.0.1.min.js") }}"></script>
    <script src="{{ asset("asset/js/jquery.collapse.js") }}"></script>
    <script src="{{ asset("asset/js/jquery.cookie.js") }}"></script>
    <script src="{{ asset("asset/js/jquery.autocomplete.min.js") }}"></script>
    <script src="{{ asset("asset/js/localdata.js") }}"></script>
    <script src="{{ asset("asset/js/excanvas.min.js") }}"></script>
    <script src="{{ asset("asset/js/jquery.jqplot.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.dateAxisRenderer.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.cursor.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.logAxisRenderer.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.canvasTextRenderer.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.canvasAxisTickRenderer.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.highlighter.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.pieRenderer.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.barRenderer.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.categoryAxisRenderer.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.pointLabels.min.js") }}"></script>
    <script src="{{ asset("asset/js/chart-plugins/jqplot.meterGaugeRenderer.min.js") }}"></script>
    
</body>
</html>
