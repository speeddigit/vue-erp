<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('{any}', function () {
    return view('layouts.app');
})->where('any', '.*');


// Route::get('store-department', function(){
//     $no_of_data = 5000;
// 	$test_data = array();
// 	for ($i = 0; $i < $no_of_data; $i++){
// 	  $test_data[$i]['department_name'] = "department";
// 	}
// 	$chunk_data = array_chunk($test_data, 1000);
// 	if (isset($chunk_data) && !empty($chunk_data)) {
// 	  foreach ($chunk_data as $chunk_data_val) {
// 	     DB::table('departments')->insert($chunk_data_val);
// 	  }
// 	}
// });
